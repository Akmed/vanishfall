package me.povnu.fall;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public void onEnable() {
		
		teleportTask();
		
		System.out.println("VanishFall has been enabled!");
		
	}

	
	public void teleportTask() {

		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			@Override
			public void run() {
				int x = (int) getConfig().getInt("Teleport.X");
				int y = (int) getConfig().getInt("Teleport.Y");
				int z = (int) getConfig().getInt("Teleport.Z");

				for (Player p : Bukkit.getOnlinePlayers()) {
					Location loc = p.getLocation();
					if (loc.getWorld().getName().equalsIgnoreCase(getConfig().getString("World.Name"))) {
						if (loc.getY() < getConfig().getInt("Teleport.TP-Y")) {
							p.teleport(p.getLocation().add(x, y, z));
							p.sendMessage(
									ChatColor.translateAlternateColorCodes('&', getConfig().getString("Messages.TP")));

						}
					}
				}

			}

		}, 1L, 1L);
	}

}
